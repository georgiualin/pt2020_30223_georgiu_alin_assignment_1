package views;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import models.Polynomial;
import models.PolynomialOperations;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.UIManager;

import controllers.Validator;

public class Viewer{

	public JFrame frame;
	private JTextField firstPolynomial;
	private JTextField secondPolynomial;
	private JTextField answerField;
	private JButton addButton;
	private JButton subtractButton;
	private JButton multiplyButton;
	private JButton divideButton;
	private JButton derivateButton;
	private JButton integrateButton;
	

	public Viewer() {
		
		initializeView();
				
		initializeButtons();
		
	    initializeButtonsActionListeners();
	}
	
	private void initializeButtonsActionListeners() {
		
		final String POL1_WARNING = "Primul polinom nu respecta formatul";
		final String POL2_WARNING = "Al doilea polinom nu respecta formatul";
		final String EMPTY_SQUARE_WARNING = "Nu s-au introdus doua polinoame";
		
	    addButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		
	    		Polynomial firstPol=null;
	    		Polynomial secondPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		String secondString = getSecondPol();
	    		
	    		if(firstString.equals("") || secondString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
				try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
					
	    		try {
					secondPol = Validator.validateInput(secondString);
				} catch (Exception e1) {
					displayMessage(POL2_WARNING);
				}
	    		if(firstPol!= null && secondPol!=null) {
	    			result = PolynomialOperations.addPolynomials(firstPol,secondPol);
	    			setAnswer(result.toString());
	    		}
	    	}
	    });
	
		subtractButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Polynomial firstPol=null;
	    		Polynomial secondPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		String secondString = getSecondPol();
	    		
	    		if(firstString.equals("") || secondString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
	    		try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
					
	    		try {
					secondPol = Validator.validateInput(secondString);
				} catch (Exception e1) {
					displayMessage(POL2_WARNING);
				}
	    		
	    		if(firstPol!= null && secondPol!=null) {
	    			result = PolynomialOperations.substractPolynomials(firstPol,secondPol);
	    			setAnswer(result.toString());
	    		}
			}
		});
		
		
		
		multiplyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial firstPol=null;
	    		Polynomial secondPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		String secondString = getSecondPol();
	    		
	    		if(firstString.equals("") || secondString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
	    		try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
					
	    		try {
					secondPol = Validator.validateInput(secondString);
				} catch (Exception e1) {
					displayMessage(POL2_WARNING);
				}
	    		
	    		if(firstPol!= null && secondPol!=null) {
	    			result = PolynomialOperations.multiplyPolynomials(firstPol,secondPol);
	    			setAnswer(result.toString());
	    		}
			}
		});
		
		
		
		divideButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial firstPol=null;
	    		Polynomial secondPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		String secondString = getSecondPol();
	    		
	    		if(firstString.equals("") || secondString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
	    		try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
					
	    		try {
					secondPol = Validator.validateInput(secondString);
				} catch (Exception e1) {
					displayMessage(POL2_WARNING);
				}
	    		
	    		if(firstPol!= null && secondPol!=null) {
	    			result = PolynomialOperations.dividePolynomials(firstPol,secondPol);
	    			setAnswer(result.toString());
	    		}
	    		
			}
		});
		
		
		
		derivateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polynomial firstPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		
	    		if(firstString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
	    		try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
	    		
	    		if(firstPol!= null) {
	    			result = PolynomialOperations.derivatePolynomial(firstPol);
	    			setAnswer(result.toString());
	    		}
			}
		});
		
		
		integrateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Polynomial firstPol=null;
	    		Polynomial result = null;
	    		String firstString = getFirstPol();
	    		
	    		if(firstString.equals("")) {
	    			displayMessage(EMPTY_SQUARE_WARNING);
	    			return;
	    		}
	    		
	    		try {
					firstPol = Validator.validateInput(firstString);
					} catch (Exception e2) {
					displayMessage(POL1_WARNING);
				}
	    		
	    		if(firstPol!= null) {
	    			result = PolynomialOperations.integratePolynomial(firstPol);
	    			setAnswer(result.toString());
	    		}
			}
		});
		
	}

	private void initializeButtons() {
		addButton = new JButton("ADD");
	    addButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
	    addButton.setBackground(new Color(204, 255, 204));
	    addButton.setBounds(35, 132, 130, 40);
		frame.getContentPane().add(addButton);
		
		subtractButton = new JButton("SUBSTRACT");
		subtractButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		subtractButton.setBackground(new Color(204, 255, 204));
		subtractButton.setBounds(35, 182, 130, 40);
		frame.getContentPane().add(subtractButton);
		
		multiplyButton = new JButton("MULTIPLY");
		multiplyButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		multiplyButton.setBackground(new Color(204, 255, 204));
		multiplyButton.setBounds(35, 232, 130, 40);
		frame.getContentPane().add(multiplyButton);
		
		divideButton = new JButton("DIVIDE");
		divideButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		divideButton.setBackground(new Color(204, 255, 204));
		divideButton.setBounds(208, 132, 130, 40);
		frame.getContentPane().add(divideButton);
		
		derivateButton = new JButton("DERIVE");
		derivateButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		derivateButton.setBackground(new Color(204, 255, 204));
		derivateButton.setBounds(208, 182, 130, 40);
		frame.getContentPane().add(derivateButton);
		
		integrateButton = new JButton("INTEGRATE");
		integrateButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		integrateButton.setBackground(new Color(204, 255, 204));
		integrateButton.setBounds(208, 232, 130, 40);
		frame.getContentPane().add(integrateButton);
		
	}
	
	private void initializeView() {
		
		frame = new JFrame();
		frame.getContentPane().setBackground(UIManager.getColor("CheckBox.light"));
		frame.setTitle("Polynomial Calculator");
		frame.setBounds(550, 100, 558, 389);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		firstPolynomial = new JTextField();
		firstPolynomial.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		firstPolynomial.setBounds(25, 30, 420, 35);
		frame.getContentPane().add(firstPolynomial);
		firstPolynomial.setColumns(10);
		
		secondPolynomial = new JTextField();
		secondPolynomial.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		secondPolynomial.setBounds(25, 86, 420, 35);
		frame.getContentPane().add(secondPolynomial);
		secondPolynomial.setColumns(10);
		
		answerField = new JTextField();
		answerField.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		answerField.setBounds(25, 297, 420, 35);
		frame.getContentPane().add(answerField);
		answerField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Enter First Polynomial");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblNewLabel.setBounds(25, 10, 169, 24);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblEnterSecondPolynomial = new JLabel("Enter Second Polynomial");
		lblEnterSecondPolynomial.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblEnterSecondPolynomial.setBounds(25, 61, 247, 34);
		frame.getContentPane().add(lblEnterSecondPolynomial);
		
		JLabel lblAnswer = new JLabel("Answer");
		lblAnswer.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblAnswer.setBounds(25, 282, 103, 17);
		frame.getContentPane().add(lblAnswer);
	}
	
	public String getFirstPol() {
		String pol1=firstPolynomial.getText();
		return pol1;
	}
	
	public String getSecondPol() {
		String pol2 = secondPolynomial.getText();
		return pol2;
	}
	
	private void setAnswer(String answer) {
		answerField.setText(answer);
	}
	
	private void displayMessage(String messageText) {
		JOptionPane.showMessageDialog(frame, messageText);
	}
}
