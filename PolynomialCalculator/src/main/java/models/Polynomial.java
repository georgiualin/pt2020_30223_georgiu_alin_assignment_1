package models;

import java.util.ArrayList;

import java.util.Collections;
public class Polynomial {
	
	ArrayList<Monomial> terms;
	
	public Polynomial() {
		
		this.terms = new ArrayList<Monomial>();
	}
	
	public ArrayList<Monomial> getTerms(){
		
		return this.terms;
	}
	
	public void addMonomToPoly(Monomial monom) {
	
		this.terms.add(monom);
	}
	
	public Monomial getMonomOfDegree(int degree) {
		
		for(Monomial monom : terms) {
			if(monom.getDegree() == degree)
				return monom;
		}
		return null;
	}
	
	public Monomial getGreatestMonomialTerm() {
		Collections.sort(terms);
		return terms.get(0);
	}
	
	public void sortMonomsInReverse() {
		Collections.sort(terms);
		Collections.reverse(terms);
	}
	
	@Override
	public String toString() {
		StringBuilder poly = new StringBuilder();
		boolean firstElem=true;
		
		Collections.sort(terms);
		for(Monomial monom : this.getTerms()) {
			
			if(monom.getCoefficient() > 0 && firstElem==false) {
				poly.append("+");
			}
			firstElem=false;
			poly.append(monom.toString());
		}
		
		if(poly.length() == 0) {
			poly.append("0");
		}
		return poly.toString();
	}
}
