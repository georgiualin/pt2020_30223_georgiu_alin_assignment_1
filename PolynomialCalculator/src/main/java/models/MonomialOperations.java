package models;

public class MonomialOperations extends PolynomialOperations{
	
	public static Monomial addMonomials(Monomial monom1,Monomial monom2) {
		
		if(monom1.getDegree()==monom2.getDegree()) {
			
			return new Monomial(monom1.getCoefficient()+monom2.getCoefficient(),monom1.getDegree());
		}
		return null;
	}
	
	public static Monomial multiplyMonomials(Monomial monom1,Monomial monom2){
		
		return new Monomial(monom1.getCoefficient()*monom2.getCoefficient(),monom1.getDegree()+monom2.getDegree());
	}
	
	public static Monomial divideMonomials(Monomial monom1,Monomial monom2) {
		
		return new Monomial(monom1.getCoefficient()/monom2.getCoefficient(),monom1.getDegree()-monom2.getDegree());
	}
	
	public static Monomial deriveMonomial(Monomial monom) {
		
		return new Monomial(monom.getCoefficient()*monom.getDegree(),monom.getDegree()-1);
	}
	
	public static Monomial integrateMonomial(Monomial monom) {
		float coefficient = monom.getCoefficient()/(monom.getDegree()+1);
		
		return new Monomial(round(coefficient,2),monom.getDegree()+1);
	}
	
	private static float round(float value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (float) tmp / factor;
	}
	
	/*
	static Monomial substractMonomials(Monomial monom1,Monomial monom2) {
		
		if(monom1.getDegree()==monom2.getDegree()) {
			
			return new Monomial(monom1.getCoefficient()-monom2.getCoefficient(),monom1.getDegree());
		}
		return null;
	}
	
	static Monomial multiplyMonomialByConstant(Monomial monom,int constant) {
		
		return new Monomial(monom.getCoefficient()*constant,monom.getDegree());
	}
	
	static Polynomial multiplyByMonome(Polynomial pol,Monomial monom) {
		Polynomial result=new Polynomial();
		for(Monomial monomI : pol.getTerms()) {
			result.addMonomToPoly(multiplyMonomials(monomI,monom));
		}
		return result;
	}
	
	*/
}
