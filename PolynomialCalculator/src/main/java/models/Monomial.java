package models;

public class Monomial implements Comparable<Monomial>{
	
	private float coefficient;
	private int degree;
	
	public Monomial(float coefficient,int degree) {
		
		this.coefficient =coefficient;
		this.degree = degree;
	}
	
	public void changeSign() {
		
		this.coefficient*=-1;
	}
	
	public int getDegree() {
		
		return this.degree;
	}
	
	public float getCoefficient() {
		
		return this.coefficient;
	}

	public int compareTo(Monomial monom) {
		
		if(monom.getDegree() < this.getDegree())
			return 1;
		else
			if(monom.getDegree() == this.getDegree())
				return 0;
			else 
				return -1;
	}
	
	@Override
	public String toString() {
		
		StringBuilder monom= new StringBuilder();
		
		if(this.degree == 0) {
			monom.append(this.getCoefficient());
		}
		else
		{
			if(this.degree > 0) {
				
				if(this.getCoefficient()==1) {
					monom.append("x^" + this.getDegree());
				}
				else
				{
					if(this.getCoefficient()==-1)
						monom.append("-x^" + this.getDegree());
					else
					{
						monom.append(this.getCoefficient() + "x^" + this.getDegree());
					}
					
				}
					
			}
		}
		return monom.toString();
	}
}
