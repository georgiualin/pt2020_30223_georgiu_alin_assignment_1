package models;
public class PolynomialOperations{
	
	
	public static Polynomial addPolynomials(Polynomial poly1,Polynomial poly2) {
		
		Polynomial resultPoly = clonePolynomial(poly1);
		for (Monomial monom1 : poly2.getTerms()) {
			Monomial monom2 = resultPoly.getMonomOfDegree(monom1.getDegree());
			if (monom2 != null) {
				Monomial sum = MonomialOperations.addMonomials(monom1, monom2);
				if (sum.getCoefficient() != 0) {
					resultPoly.addMonomToPoly(sum);
				}
				resultPoly.getTerms().remove(monom2);
			} else {
				resultPoly.addMonomToPoly(monom1);
			}
		}
		resultPoly.sortMonomsInReverse();
		return resultPoly;
	}
	
	public static Polynomial substractPolynomials(Polynomial poly1,Polynomial poly2) {
		return addPolynomials(poly1,invertSigns(poly2));
	}
	
	public Polynomial divideByConstant(Polynomial poly1,Polynomial poly2) {
		Polynomial resultPoly = new Polynomial();
		for(Monomial monom : poly1.getTerms()) {
			resultPoly.addMonomToPoly(MonomialOperations.divideMonomials(monom,poly2.getGreatestMonomialTerm()));
		}
		return resultPoly;
	}
	
	public static Polynomial multiplyPolynomials(Polynomial poly1, Polynomial poly2) {
		Polynomial resultPoly = new Polynomial();
		for (Monomial monom1 : poly2.getTerms()) {
			Polynomial addPoly = new Polynomial();
			for (Monomial monom2 : poly1.getTerms()) {
				if (monom1.getCoefficient() != 0 && monom2.getCoefficient() != 0) {
					addPoly.addMonomToPoly(MonomialOperations.multiplyMonomials(monom1, monom2));
				}
			}
			resultPoly = addPolynomials(resultPoly, addPoly);
		}
		return resultPoly;
	}
	
	public static Polynomial dividePolynomials(Polynomial poly1, Polynomial poly2) {
		
		Polynomial resultPoly = new Polynomial();
		for (Monomial monom1 : poly2.getTerms()) {
			Polynomial addPoly = new Polynomial();
			for (Monomial monom2 : poly1.getTerms()) {
				if (monom1.getCoefficient() != 0 && monom2.getCoefficient() != 0) {
					addPoly.addMonomToPoly(MonomialOperations.divideMonomials(monom1, monom2));
				}
			}
			resultPoly = addPolynomials(resultPoly, addPoly);
		}
		return resultPoly;
	}
	
	public static Polynomial derivatePolynomial(Polynomial pol) {
		
		Polynomial poly = new Polynomial();
		for (Monomial monom : pol.getTerms()) {
			poly.addMonomToPoly(MonomialOperations.deriveMonomial(monom));
		}
		return poly;
	}
	
	public static Polynomial integratePolynomial(Polynomial pol) {
		
		Polynomial poly = new Polynomial();
		for (Monomial monom : pol.getTerms()) {
			poly.addMonomToPoly(MonomialOperations.integrateMonomial(monom));
		}
		return poly;
	}
	
	public static Polynomial invertSigns(Polynomial pol) {
		
		Polynomial invertPoly = new Polynomial();
		invertPoly=clonePolynomial(pol);
		for (Monomial term : invertPoly.getTerms()) {
			term.changeSign();
		}
		return invertPoly;
	}
	
	private static Polynomial clonePolynomial(Polynomial pol) {
		
		Polynomial polyClone = new Polynomial();
		for (Monomial monom : pol.getTerms()) {
			Monomial monomTerm = new Monomial(monom.getCoefficient(),monom.getDegree());
			polyClone.addMonomToPoly(monomTerm);
		}
		return polyClone;
	}

	
}