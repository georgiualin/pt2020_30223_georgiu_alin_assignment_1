package controllers;

import models.Polynomial;
import models.PolynomialOperations;
import views.Viewer;

public class MainController {

	public static void main(String[] args) throws Exception {
		
		Viewer window = new Viewer();
		window.frame.setVisible(true);

		Polynomial firstPol = null;
		Polynomial secondPol = null;

		final String firstString = "4x^2+x+1";
		final String secondString = "x^4+x^3+2x^2+3";
		firstPol = Validator.validateInput(firstString);
		secondPol = Validator.validateInput(secondString);
		System.out.println("Polinom 1: "+firstPol.toString());
		System.out.println("Polinom 2: "+secondPol.toString());

		System.out.println("\nADD :" + PolynomialOperations.addPolynomials(firstPol, secondPol).toString() + "\n");

		System.out.println("SUB :" + PolynomialOperations.substractPolynomials(firstPol, secondPol).toString() + "\n");

		System.out.println(	"Multiply :" + PolynomialOperations.multiplyPolynomials(firstPol, secondPol).toString() + "\n");

		System.out.println("Derive : " + PolynomialOperations.derivatePolynomial(firstPol).toString() + "\n");

		System.out.println("Intergrate : " + PolynomialOperations.integratePolynomial(firstPol).toString() + "\n");

	}
}
