package controllers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.Monomial;
import models.Polynomial;

public class Validator {

	public static Polynomial validateInput(String polyString) throws Exception {

		final String PATTERN = "([\\+\\-][\\d]*)([x]*([\\^][\\d]+)*)";

		String poly = polyString.substring(0, 1);
		Pattern myPattern = null;
		Matcher myMatcher = null;

		if (!poly.equals("-")) {
			poly = "+" + polyString;
		} else {
			poly = polyString;
		}

		if (!poly.matches("(" + PATTERN + ")*")) {
			throw new Exception("Datele introduse nu sunt corecte!");
		}

		Polynomial resultPoly = new Polynomial();
		myPattern = Pattern.compile(PATTERN);
		myMatcher = myPattern.matcher(poly);

		while (myMatcher.find()) {

			if (!myMatcher.group(0).isEmpty()) {
				resultPoly.addMonomToPoly(stringToMonomial(myMatcher));
			}
		}
		return resultPoly;
	}

	static Monomial stringToMonomial(Matcher myMatcher) {

		int degree, coefficient;
		degree = coefficient = 0;

		if (myMatcher.group(3) != null) {
			degree = Integer.parseInt(myMatcher.group(3).substring(1, 2));
		} else {
			if (myMatcher.group(2).length() == 1) {
				degree = 1;
			} else {
				degree = 0;
			}
		}

		if (myMatcher.group(1).isEmpty() || myMatcher.group(1).equals("+")) {
			if (!myMatcher.group(2).equals("")) {
				coefficient = 1;
			}
		} else {
			if (!myMatcher.group(1).equals("-")) {
				coefficient = Integer.parseInt(myMatcher.group(1));

			} else {
				coefficient = -1;
			}
		}
		return new Monomial(coefficient, degree);
	}
}
