/*package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import models.Monomial;
import models.MonomialOperations;

public class MonomialTest {

	public void test() {
		Monomial monom1 = new Monomial(3, 2);
		Monomial monom2 = new Monomial(4, 2);

		// ADD
		if (monom1.getDegree() == monom2.getDegree()) {
			Monomial monomResult = MonomialOperations.addMonomials(monom1, monom2);
			assertEquals("7.0x^2", monomResult.toString());
		}

		// Multiplicate
		assertEquals("12.0x^4", MonomialOperations.multiplyMonomials(monom1, monom2).toString());

		// Derive
		assertEquals("8.0x^1", MonomialOperations.deriveMonomial(monom2).toString());

		// Integrate
		assertEquals("x^3", MonomialOperations.integrateMonomial(monom1).toString());
	}

}
*/