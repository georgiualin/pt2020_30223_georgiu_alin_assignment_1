/*package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import models.Monomial;
import models.Polynomial;

public class PolynomialTest {

	public void test() {
		Polynomial poly = new Polynomial();
		Polynomial resultPoly = new Polynomial();
		
		poly.addMonomToPoly(new Monomial(5,5));
		poly.addMonomToPoly(new Monomial(-4,4));
		poly.addMonomToPoly(new Monomial(3,3));
		poly.addMonomToPoly(new Monomial(-2,2));
		poly.addMonomToPoly(new Monomial(1,1));
		poly.addMonomToPoly(new Monomial(-1,0));
		
		assertEquals("-1.0+x^1-2.0x^2+3.0x^3-4.0x^4+5.0x^5",poly.toString());
		
		//InvertSigns
		resultPoly = PolynomialOperations.invertSigns(poly);
		assertEquals("1.0-x^1+2.0x^2-3.0x^3+4.0x^4-5.0x^5",resultPoly.toString());
		
		//Integrate
		resultPoly = PolynomialOperations.integratePolynomial(poly);
		assertEquals("-x^1+0.5x^2-0.67x^3+0.75x^4-0.8x^5+0.83x^6",resultPoly.toString());
		
		//Derivate
		resultPoly = PolynomialOperations.derivatePolynomial(poly);
		assertEquals("+1.0-4.0x^1+9.0x^2-16.0x^3+25.0x^4",resultPoly.toString());
		
		//ADD
		resultPoly = PolynomialOperations.addPolynomials(poly,resultPoly);
		assertEquals("-3.0x^1+7.0x^2-13.0x^3+21.0x^4+5.0x^5",resultPoly.toString());
		
		//SUB
		resultPoly = PolynomialOperations.substractPolynomials(poly,resultPoly);
		assertEquals("-1.0+4.0x^1-9.0x^2+16.0x^3-25.0x^4",resultPoly.toString());
		//Multiply
		resultPoly = PolynomialOperations.multiplyPolynomials(poly,resultPoly);
		assertEquals("1.0-5.0x^1+15.0x^2-36.0x^3+75.0x^4-105.0x^5+154.0x^6-184.0x^7+180.0x^8-125.0x^9",resultPoly.toString());
		}
}*/
